package com.huqizhi.eurekaserver;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.eureka.server.EnableEurekaServer;

@EnableEurekaServer
@SpringBootApplication
public class EurekaServerApplication {
    public static void main(String[] args) {
        System.out.println("Hello Mr.Hu! UTC 时间：2024-05-10T01:43:18.901Z");
        SpringApplication.run(EurekaServerApplication.class, args);
    }

}