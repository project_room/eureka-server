# 指定基础镜像
FROM bitnami/java:1.8

LABEL maintainer=huqizhia@qq.com \
      org.opencontainers.image.title=eureka \
      org.opencontainers.image.version=spring-boot-2.7.18 \
      org.opencontainers.image.created=2024-02-02T17:11:31Z


# 拷贝ava项目的包
COPY ./eureka-server_2024-02-02_08-58-18.jar /tmp/app.jar

# 配置时区
ENV TZ=Asia/Shanghai
# 配置编码
ENV LANG=C.UTF-8

# 暴露端口
EXPOSE 8080
# 入口，java项目的启动命令
ENTRYPOINT java -jar /tmp/app.jar